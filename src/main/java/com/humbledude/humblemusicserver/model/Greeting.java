package com.humbledude.humblemusicserver.model;

/**
 * Created by keunhui.park on 2016. 7. 23..
 */
public class Greeting {

  private final long id;
  private final String content;

  public Greeting(long id, String content) {
    this.id = id;
    this.content = content;
  }

  public long getId() {
    return id;
  }

  public String getContent() {
    return content;
  }
}
