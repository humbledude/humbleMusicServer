package com.humbledude.humblemusicserver.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by keunhui.park on 2016. 7. 23..
 */
public class GreetingTest {

  private Greeting model;

  private final long id = 1;
  private final String content = "test";


  @Before
  public void setup() {
    model = new Greeting(id, content);

  }

  @Test
  public void test() {
    Assert.assertEquals(id, model.getId());
    Assert.assertEquals(content, model.getContent());

  }
}
